Node.js is a platform built on Chrome's JavaScript runtime for easily building fast and scalable network applications. Node.js uses an event-driven, non-blocking asynchronous I/O model that makes it lightweight and efficient, perfect for data-intensive real-time applications that run across distributed devices.


The prototype property is an object created by JS for every Fn instance created.

Design patterns: https://scotch.io/bar-talk/4-javascript-design-patterns-you-should-know 
—>
The design patterns in question include the following:

1.Module
2.Prototype
  Delegation (i.e., the prototype chain).
Concatenative (i.e. mixins, `Object.assign()`).
Functional (Not to be confused with functional programming. A function used to create a closure for private state/encapsulation).
3.Observer
4.Singleton


Asynchronous programming means that the engine runs in an event loop. When a blocking operation is needed, the request is started, and the code keeps running without blocking for the result. When the response is ready, an interrupt is fired, which causes an event handler to be run, where the control flow continues. In this way, a single program thread can handle many concurrent operations.





The Event Loop is a queue of callback functions. When an async function executes, the callback function is pushed into the queue. The JavaScript engine doesn't start processing the event loop until the code after an async function has executed

https://developer.mozilla.org/en/docs/Web/JavaScript/EventLoop
http://stackoverflow.com/questions/21607692/understanding-the-event-loop


CLOSURE
 Closure is wen a fn “remembers” its lexical scope even wen the fn is executed outside that lexical scope.
Or
Closures are created whenever a variable that is defined outside the current scope is accessed from within some inner scope.
Or
In Javascript functions can access variables in their outer scope:










Functional Programming is a style of writing programs by simply composing a set of functions.
Essentially, FP asks us to wrap virtually everything in functions, write lots of small reusable functions and simply call them one after the other to get the result like: (func1.func2.func3) or in a compose fashion, like: func1(func2(func3())).
Functional programming (often abbreviated FP) is the process of building software by composing pure functions, avoiding shared state, mutable data, and side-effects. Functional programming is declarative rather than imperative, and application state flows through pure functions. Contrast with object oriented programming, where application state is usually shared and colocated with methods in objects.
Functional programming is a programming paradigm, meaning that it is a way of thinking about software construction based on some fundamental, defining principles (listed above). Other examples of programming paradigms include object oriented programming and procedural programming.

A pure function is a function where the return value is only determined by its input values, without observable side effects. This is how functions in math work: Math.cos(x) will, for the same value of x , always return the same result
A pure function is a function which:

Given the same input, will always return the same output.
Produces no side effects.
Relies on no external mutable state.
A pure function doesn't depend on and doesn't modify the states of variables out of its scope. Concretely, that means a pure function always returns the same result given same parameters. Its execution doesn't depend on the state of the system.Pure functions are a pillar of functional programming.
A pure function doesn’t depend on and doesn’t modify the states of variables out of its scope.

Concretely, that means a pure function always returns the same result given same parameters. Its execution doesn’t depend on the state of the system

For example an impure function that has a side effect on a variable outside of its own scope:


var count = 0;

function increaseCount(val) {
    count += val;
}
Or a function that returns different values for the same input because it evaluates a variable that is not given as parameter:

var count = 0;

function getSomething() {
    return count > 0;
}

Function composition is the process of combining two or more functions in order to produce a new function or perform some computation. For example, the composition f . g (the dot means “composed with”) is equivalent to f(g(x)) in JavaScript.

What is a higher order function?
Functions that operate on other functions, either by taking them as arguments or by returning them, are called higher-order functions. If you have already accepted the fact that functions are regular values, there is nothing particularly remarkable about the fact that such functions exist. The term comes from mathematics, where the distinction between functions and other values is taken more seriously.

A callback function is a function that you pass to someone and let them call it at some point of time. I think people just read the first sentence of the wiki definition: acallback is a reference to executable code, or a piece of executable code, that is passed as an argument to other code.


Currying refers to the process of transforming a function with multiple arity into the same function with less arity. The curried effect is achieved by binding some of the arguments to the first function invoke, so that those values are fixed for the next invocation. Here’s an example of what a curried function looks like:
Briefly, currying is a way of constructing functions that allows partial application of a function’s arguments. What this means is that you can pass all of the arguments a function is expecting and get the result, or pass a subset of those arguments and get a function back that’s waiting for the rest of the arguments. It really is that simple.


Currying is elemental in languages such as Haskell and Scala, which are built around functional concepts. JavaScript has functional capabilities, but currying isn’t built in by default (at least not in current versions of the language). But we already know some functional tricks, and we can make currying work for us in JavaScript, too.


var greet = function(greeting, name) { console.log(greeting + ", " + name);};greet("Hello", "Heidi"); //"Hello, Heidi"



down voteaccepted
Currying is when you break down a function that takes multiple arguments into a series of functions that take part of the arguments. Here's an example in JavaScript:


function add (a, b) {
  return a + b;
}

add(3, 4); returns 7
This is a function that takes two arguments, a and b, and returns their sum. We will now curry this function:


function add (a) {
  return function (b) {
    return a + b;
  }
}
This is a function that takes one argument, a, and returns a function that takes another argument, b, and that function returns their sum.


add(3)(4);

var add3 = add(3);

add3(4);




Composition is simply when a class is composed of other classes; or to say it another way, an instance of an object has references to instances of other objects.

object composition (not to be confused with function composition) is a way to combine simple objects or data types into more complex ones.

In this case, we want to combine two methods to form a composite that we can refer to directly. But let’s examine what’s happening under the hood ourselves, first.

Here’s an example of composition:

var angryAndMad = compose(getMad, getAngry);
var johnIsAngryAndMad = angryAndMad('John');
var aliceIsAngryAndMad = angryAndMad('Alice');
Inheritance is when a class inherits methods and properties from another class.

What is a thunk?

ALGOL thunks in 1961

thunk is a function that encapsulates synchronous or asynchronous code inside.

thunk accepts only one callback function as an arguments, which is a CPS function.

thunk returns another thunk function after being called, for chaining operations.

thunk passes the results into a callback function after being excuted.

If the return value of callback is a thunk function, then it will be executed first and its result will be send to another thunk for excution, or it will be sent to another new thunk function as the value of the computation.

Demo

const thunk = require('thunks')()
const fs = require('fs')
const size = thunk.thunkify(fs.stat)

// generator
thunk(function * () {

  // sequential
  console.log(yield size('.gitignore'))
  console.log(yield size('thunks.js'))
  console.log(yield size('package.json'))
})(function * (error, res) {
  //parallel
  console.log(yield thunk.all([
    size('.gitignore'),
    size('thunks.js'),
    size('package.json')
  ]))
})()





Introduction to Contexts in React.js (https://blog.jscrambler.com/react-js-communication-between-components-with-contexts/)
Contexts are a feature that will eventually be released in React.js - however, they exist today in an undocumented form. I spent an afternoon looking into the present implementation and was frustrated by the lack of documentation (justified, as it is a changing feature). I've pieced together a few code examples that I found helpful.

In React.js a context is a set of attributes that are implicitly passed down from an element to all of its children and grandchildren.

Why would you use a context rather than explicitly passing properties down to child elements? There are a few different reasons. You may be building a widget with a large child tree where child elements have the ability to drastically affect the widget's overall state. If you're not using the Flux pattern (where the parent widget listens to Stores that are affected by Action Creators invoked by the child elements), the idiomatic way to do this is to pass callbacks that affect the overall widget through props - this can be a bit awkward when you are passing a callback down several levels.

Another situation where contexts are useful is where you are doing server-side rendering - in this case data comes in that is uniquely associated with the user (e.g. session information). If your elements require session information this needs to be passed down from parent to child which gets inelegant very quickly.

Update (2/19/2015): React.withContext is deprecated as of React 0.13-alpha. You should investigate getChildContext with a wrapper component for future-facing code. Contexts themselves are not going away - they are [planned for React 1.0]((https://facebook.github.io/react/blog/2014/03/28/the-road-to-1.0.html#context) and at ReactConf 2015 the React team confirmed that the context feature was staying, with some cool examples of how contexts have been used in the past.

React.withContext


React.withContext will execute a callback with a specified context dictionary. Any rendered React element inside this callback has access to values from the context.







React and ReactDOM were only recently split into two different libraries. Prior to v0.14, all ReactDOM functionality was part of React. This may be a source of confusion, since any slightly dated documentation won't mention the React / ReactDOM distinction.

As the name implies, ReactDOM is the glue between React and the DOM. Often, you will only use it for one single thing: mounting with ReactDOM.render(). Another useful feature of ReactDOM is ReactDOM.findDOMNode() which you can use to gain direct access to a DOM element. (Something you should use sparingly in React apps, but it can be necessary.) If your app is "isomorphic", you would also use ReactDOM.renderToString() in your back-end code.

For everything else, there's React. You use React to define and create your elements, for lifecycle hooks, etc. i.e. the guts of a React application.

The reason React and ReactDOM were split into two libraries was due to the arrival of React Native. React contains functionality utilised in web and mobile apps. ReactDOM functionality is utilised only in web apps. [UPDATE: Upon further research, it's clear my ignorance of React Native is showing. Having the React package common to both web and mobile appears to be more of an aspiration than a reality right now. React Native is at present an entirely different package.]


See the blog post announcing the v0.14 release:https://facebook.github.io/react/blog/2015/10/07/react-v0.14.html





First of all - the Virtual DOM was not invented by React, but React uses it and provides it for free.

The Virtual DOM is an abstraction of the HTML DOM. It is lightweight and detached from the browser-specific implementation details. Since the DOM itself was already an abstraction, the virtual DOM is, in fact, an abstraction of an abstraction.

MUST READ:
http://reactkungfu.com/2015/10/the-difference-between-virtual-dom-and-dom/
https://www.accelebrate.com/blog/the-real-benefits-of-the-virtual-dom-in-react-js/
http://stackoverflow.com/questions/21109361/why-is-reacts-concept-of-virtual-dom-said-to-be-more-performant-than-dirty-mode


WeakMaps provide a way to extend objects from the outside without interfering with garbage collection. Whenever you want to extend an object but can't because it is sealed - or from an external source - a WeakMap can be applied.

A WeakMap is a map (dictionary) where the keys are weak - that is, if all references to the key are lost and there are no more references to the value - the value can be garbage collected. Let's show this first through examples, then explain it a bit and finally finish with a real use.

https://ilikekillnerds.com/2015/02/what-are-weakmaps-in-es6/










JavaScript Promises use callback functions actually to specify what to do after a Promise has been resolved or rejected, so the two are not fundamentally different. The main idea behind Promises is to take callbacks - especially nested callbacks where you want to perform a series of actions, each one after the resolution of the former- and “flatten out” the code required to do this. For example, the below uses JavaScript Promises to load a series of images and perform the desired action after each image load:

function fetchImage(url){
return new Promise(function(resolve, reject){
var img = new Image()
img.onload = function(){
resolve(url)
}
img.onerror = function(){
reject(url)
}
img.src = url
})
}
 
fetchImage('image1.png').then(function(url){
console.log(url + ' downloaded!')
return fetchImage('image2.png')
}).then(function(url){
console.log(url + ' downloaded!')
return fetchImage('image3.png')
}).then(function(url){
console.log(url + ' downloaded!')
return fetchImage('image4.png')
}).then(function(url){
console.log(url + ' downloaded!')
})
 
//Console log:
// image1.png downloaded!
// image2.png downloaded!
// image3.png downloaded!
// image4.png downloaded!


Promises provide a more succinct and clear way of representing sequential asynchronous operations in javascript. They are effectively a different syntax for achieving the same effect as callbacks. The advantage is increased readability. Something like this


aAsync()
  .then(bAsync)
  .then(cAsync)
  .done(finish);
is much more readable then the equivalent of passing each of those individual functions as callbacks, like

Async(function(){
    return bAsync(function(){
        return cAsync(function(){
            finish()
        })
    })
})




You are confused about promises and Ajax calls. They are kind of like apples and knives. You can cut an apple with knife and the knife is a tool that can be applied to an apple, but the two are very different things.

Promises are a tool for managing asynchronous operations. They keep track of when asynchronous operations complete and what their results are and let you coordinate that completion and those results (including error conditions) with other code or other asynchronous operations. They aren't actually asynchronous operations in themselves. An Ajax call is a specific asynchronous operation that can be used with with a traditional callback interface or wrapped in a promise interface.

So what's the difference between them? And when would be best to use one instead of the other?

An Ajax call is a specific type of asynchronous operation. You can make an Ajax call either with a traditional callback using the XMLHttpRequest interface or you can make an Ajax call (in modern browsers), using a promise with the fetch() interface.

Recently I encountered a promise which had an AJAX in its body. Why put an async operation inside an async operation? That's like putting a bread loaf in a bread sandwich.

You didn't show the specific code you were talking about, but sometimes you want to start async operation 1 and then when that async operation is done, you want to them start async operation 2 (often using the results of the first one). In that case, you will typically nest one inside the other.

Your code example here:


function threadsGet() {
    return new Promise((resolve, reject) => {
      $.getJSON('api/threads')
        .done(resolve)
        .fail(reject);
      })
}
is considered a promise anti-pattern. There's no reason to create a new promise here because $.getJSON() already returns a promise which you can return. You can just do this instead:


function threadsGet() {
    return $.getJSON('api/threads');
}
Or, if you want to "cast" the somewhat non-standard jQuery promise to a standard promise, you can do this:

function threadsGet() {
    return Promise.resolve($.getJSON('api/threads'));
}


http://blog.parse.com/learn/engineering/whats-so-great-about-javascript-promises/