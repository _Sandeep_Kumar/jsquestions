/**
 * Created by pratheesh.pm on 09/01/17.
 */


var _ = {
    each : function(obj, callback) {
        if (obj.constructor === Array) {
            for (var i = 0; i < obj.length; i++) {
                callback(obj[i], i, obj);
            }
        }
        if (obj.constructor === Object) {
            for (var key in obj) {
                callback(obj[key], key, obj);
            }
        }
    },
    map: function(obj, fn) {
        var mapped = [];
        _.each(obj, function(item) {
            mapped.push(fn(item));
        });
        return mapped;
    },
    reduce: function(obj, fn, initVal) {
        var initializing = (initVal === undefined);
        _.each(obj, function(item) {
            if (initializing) {
                initVal = item;
                initializing = false;
            } else {
                initVal = fn(initVal, item);
            }
        });
        return initVal;
    },
    reduceRight: function(obj, fn, initVal) {
        (obj.constructor === Array) && obj.reverse();
        return _.reduce(obj,fn,initVal);
    },
    find: function(obj, fn){
        var i;
        for (i = 0; i < obj.length; i++) {
            if(fn(obj[i])){
                break;
            }
        }
        return obj[i];
    },
    filter: function(obj, fn){
        var mapped = [],res;
        _.each(obj, function(item) {
            var res = fn(item);
            res && mapped.push(item);
        });
        return mapped;
    },
    //_.where, _.findWhere, _reject _some _contains are very similar to teh find and filter
    every: function(collection, fn) {
        return _.reduce(collection, function(val, i) {
            return val && fn(i);
        }, true);
    },


    //bored... picking up random which i feel they are a bit challenging

    last: function(array, n) {
        if (n === undefined) {
            return array[array.length-1];
        } else {
            return array.slice(array.length-n);
        }
    },

    uniq: function(array) {
        var uniqueStorage = {};
        var results = [];

        _.each(array, function(item) {
            uniqueStorage[item] = item;
        });

        _.each(uniqueStorage, function(prop) {
            results.push(prop);
        });

        return results;
    },
    extend: function(obj) {
        _.each(arguments, function(argObject) {
            _.each(argObject, function(value, key) {
                obj[key] = value;
            });
        });
        return obj;
    },
    defaults: function(obj) {
        _.each(arguments, function(argObject) {
            _.each(argObject, function(value, key) {
                if (obj[key] === undefined) {
                    obj[key] = value;
                }
            });
        });
        return obj;
    },
    once:function(func) {
        var alreadyCalled = false;
        var result;

        return function () {
            if (!alreadyCalled) {
                result = func.apply(this, arguments);
                alreadyCalled = true;
            }
            return result;
        }
    },
	 memoize: function(func) {
	    var storage = {};
	    return function() {
	      var arg = JSON.stringify(arguments);
	      if (!storage[arg]) {
	        storage[arg] = func.apply(this, arguments);
	      }
	      return storage[arg];
	    };
	  },

	  
	// Returns a function, that, as long as it continues to be invoked, will not
	// be triggered. The function will be called after it stops being called for
	// N milliseconds. If `immediate` is passed, trigger the function on the
	// leading edge, instead of the trailing.

	 debounce: function (func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	}

};

_.each({one: 1, two: 2, three: 3}, function(val, index){
    console.log(val,index)
});

_.each([100, 200, 300], function(val, index){
    console.log("val->" +val+" indes->"+index)
});
console.log(_.map([1, 2, 3], function(num){ return num * 3; }));

console.log(_.reduce([1, 2, 3], function(memo, num){
    console.log("memo-->"+memo+" memo+num->"+memo+"+"+num)
    return memo + num;
},20));

//reduceRight
var list = [[0, 1], [2, 3], [4, 5]];
var flat = _.reduceRight(list, function(a, b) {
    console.log(a,b)
    return a.concat(b); }, []);
console.log("final-->",flat);


//find
//filter
var even = _.find([1, 2, 3, 4, 5, 6], function(num){ return num % 2 == 0; });
console.log(even)


//filter
var even = _.filter([1, 2, 3, 4, 5, 6], function(num){ return num % 2 == 0; });
console.log(even)

//every
console.log(_.every([2, 4, 3], function(num) { return num % 2 == 0; }))

//last
console.log(_.last([5, 4, 3, 2, 1],3))

//uniq
console.log(_.uniq([1, 2, 1, 4, 1, 3]))

var iceCream = {flavor: "chocolate"};
_.defaults(iceCream, {flavor: "vanilla", sprinkles: "lots"});
//=> {flavor: "chocolate", sprinkles: "lots"}


//memoize
var fibonacci = _.memoize(function (n) {
  return n < 2 ? n: fibonacci(n - 1) + fibonacci(n - 2);
});
var factorial = _.memoize(function(n) {
    if(n < 2) return 1;
    return n * factorial(n-1);
});
console.log(fibonacci(5))

