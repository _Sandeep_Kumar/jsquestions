
Function.prototype.bind = function () {
    var fn = this,
        args = Array.prototype.slice.call(arguments); 
        console.log("b4-->",args,args[0],typeof args);

	var object = args.shift();
        console.log(" object-->",object,args);
    return function () {
        return fn.apply(object,
        args.concat(Array.prototype.slice.call(arguments)));
    };
};

//usage:
var myObject = {
	x: "my value"
};

var foo = function(){
	console.log(this.x);
}.bind(myObject);

foo();
